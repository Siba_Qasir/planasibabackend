<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmployeesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [AuthController::class, 'login']);
Route::get('logout', [AuthController::class, 'logout'])->middleware(['auth:api']);

Route::group(['prefix' => 'company'], function () {
    Route::get('/', [CompaniesController::class, 'index'])->middleware(['auth:api', "role:admin"]);
    Route::get('/{id}', [CompaniesController::class, 'show'])->middleware(['auth:api', "role:admin"]);
    Route::post('/', [CompaniesController::class, 'store'])->middleware(['auth:api', "role:admin"]);
    Route::put('/{id}', [CompaniesController::class, 'update'])->middleware(['auth:api', "role:admin"]);
    Route::delete('/{id}', [CompaniesController::class, 'destroy'])->middleware(['auth:api', "role:admin"]);
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('/', [EmployeesController::class, 'index'])->middleware(['auth:api', "role:admin"]);
    Route::get('/{id}', [EmployeesController::class, 'show'])->middleware(['auth:api', "role:admin"]);
    Route::post('/', [EmployeesController::class, 'store'])->middleware(['auth:api', "role:admin"]);
    Route::put('/{id}', [EmployeesController::class, 'update'])->middleware(['auth:api', "role:admin"]);
    Route::delete('/{id}', [EmployeesController::class, 'destroy'])->middleware(['auth:api', "role:admin"]);
});
