<?php

namespace App\Services\Employee;

use App\Models\Employee;
use App\Services\Service;

class EmployeeService extends Service
{
    function __construct()
    {
        $this->model = Employee::class;
    }

    /**
     * @param $criteria
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */

    function resolveCriteria($data = [])
    {
        $query = $this->model::Query();

        if (array_key_exists('columns', $data))
            $query = $query->select($data['columns']);

        if (array_key_exists('id', $data))
            $query = $query->where('id', $data['id']);

        if (array_key_exists('first_name', $data))
            $query = $query->where('first_name', 'LIKE', "%" . $data['first_name'] . "%");

        if (array_key_exists('last_name', $data))
            $query = $query->where('last_name', 'LIKE', "%" . $data['last_name'] . "%");

        if (array_key_exists('email', $data))
            $query = $query->where('email', 'LIKE', "%" . $data['email'] . "%");

        if (array_key_exists('phone', $data))
            $query = $query->where('phone', 'LIKE', "%" . $data['phone'] . "%");

        if (array_key_exists('company_id', $data) && $data['company_id'])
            $query = $query->where('company_id', $data['company_id']);

        if (array_key_exists('keyword', $data) && $data['keyword']){
            $query = $query->where('first_name', 'LIKE', "%" . $data['keyword'] . "%")
                        ->orWhere('last_name', 'LIKE', "%" . $data['keyword'] . "%")
                        ->orWhere('email', 'LIKE', "%" . $data['keyword'] . "%")
                        ->orWhere('phone', 'LIKE', "%" . $data['keyword'] . "%");
        }

        if (array_key_exists('order_by', $data)) {
            if(!array_key_exists('order_dir', $data))
                $data['order_dir'] = 'ASC';
            if (in_array($data['order_by'], (new $this->model)->getFillable()) && in_array($data['order_dir'], ['ASC', 'DESC', 'asc' , 'desc'])) {
                $query = $query->orderBy($data['order_by'], $data['order_dir']);
            }
        } 

        return $query;
    }
}