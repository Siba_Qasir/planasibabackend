<?php

namespace App\Services\Company;

use App\Models\Company;
use App\Services\Service;

class CompanyService extends Service
{
    function __construct()
    {
        $this->model = Company::class;
        $this->files = ['logo'];
    }

    /**
     * @param $criteria
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */

    function resolveCriteria($data = [])
    {
        $query = $this->model::Query();

        if (array_key_exists('columns', $data))
            $query = $query->select($data['columns']);

        if (array_key_exists('id', $data))
            $query = $query->where('id', $data['id']);

        if (array_key_exists('name', $data))
            $query = $query->where('name', 'LIKE', "%" . $data['name'] . "%");

        if (array_key_exists('email', $data))
            $query = $query->where('email', 'LIKE', "%" . $data['email'] . "%");

        if (array_key_exists('website', $data))
            $query = $query->where('website', 'LIKE', "%" . $data['website'] . "%");

        if (array_key_exists('keyword', $data)){
            $query = $query->where('name', 'LIKE', "%" . $data['keyword'] . "%")
                        ->orWhere('email', 'LIKE', "%" . $data['keyword'] . "%")
                        ->orWhere('website', 'LIKE', "%" . $data['keyword'] . "%");
        }

        if (array_key_exists('order_by', $data)) {
            if(!array_key_exists('order_dir', $data))
                $data['order_dir'] = 'ASC';
            if (in_array($data['order_by'], (new $this->model)->getFillable()) && in_array($data['order_dir'], ['ASC', 'DESC', 'asc' , 'desc'])) {
                $query = $query->orderBy($data['order_by'], $data['order_dir']);
            }
        } 

        return $query;
    }
}