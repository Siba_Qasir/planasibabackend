<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

abstract class Service
{
    protected $model;
    protected $files;

    abstract protected function resolveCriteria($data);

    public function create($dataIn, &$model)
    {
        if (is_array($this->files)) {
            foreach ($this->files as $key => $val) {
                if (array_key_exists($val, $dataIn)) {
                    $dataIn[$val] = Storage::disk('public')->putFile('uploads', $dataIn[$val]);
                }
            }
        }
        $this->mapDataModel($dataIn, $model);
        $model->save();
        $model->refresh();
    }

    public function update($dataIn, &$model)
    {
        if (is_array($this->files)) {
            foreach ($this->files as $key => $val) {
                if (array_key_exists($val, $dataIn)) {
                    if (!is_null($model->$val) && Storage::disk("public")->exists($model->$val)) {
                        Storage::disk("uploads")->delete($model->$val);
                    }
                    $dataIn[$val] = Storage::disk('public')->putFile('uploads', $dataIn[$val]);
                }
            }
        }
        $this->mapDataModel($dataIn, $model);
        $model->save();
        $model->refresh();
    }

    public function delete($id)
    {
        $result = $this->getOne($id);
        if (is_array($this->files)) {
            foreach ($this->files as $key => $val) {
                if (!is_null($result->$val) && Storage::disk("public")->exists($result->$val)) {
                    Storage::disk("uploads")->delete($result->$val);
                }
            }
        }
        $result->delete();
    }

    public function getList($criteria = [])
    {
        return $this->resolveCriteria($criteria)->paginate($criteria['limit']);
    }

    public function getCount($criteria = [])
    {
        if (array_key_exists('limit', $criteria))
            unset($criteria['limit']);

        if (array_key_exists('orderBy', $criteria))
            unset($criteria['orderBy']);

        return $this->resolveCriteria($criteria)->get()->count();
    }

    public function getOne($id)
    {
        return $this->resolveCriteria(['id' => $id])->firstOrFail();
    }

    public function mapDataModel($data, &$model)
    {
        foreach ($model->getFillable() as $key => $value) {
            if (array_key_exists($value, $data)) {
                if ($model->enums && in_array($value, $model->enums)) {
                    $model->$value = strval($data[$value]);
                } else {
                    $model->$value = $data[$value];
                }
            }
        }
    }
}