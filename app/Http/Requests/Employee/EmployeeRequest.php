<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\Company;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'sometimes|email|unique:employees,email',
            'company_id' => 'required|integer|exists:' . (new Company())->getTable() . ',id'
        ];
        return $rules;
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'success' => false,
            'message' => 'Validation Error',
            'data' => $validator->errors(),
            "count" => count($validator->errors()),
            "status" => 422
        ];
        throw new HttpResponseException(response()->json(
            $data, 422));
    }
}
