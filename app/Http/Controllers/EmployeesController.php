<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Facades\Employee\EmployeeService;
use App\Http\Requests\Employee\EmployeeRequest;
use App\Http\Requests\Employee\UpdateEmployeeRequest;
use App\Http\Resources\Employee\EmployeeResource;
use DB;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $criteria = $request->all();
        if(!array_key_exists('limit', $criteria))
            $criteria['limit'] = 10;
        try{
            $results = EmployeeService::getList($criteria);           
            $count = EmployeeService::getCount($criteria);

            return EmployeeResource::collection($results)->response()->header('X-Pagination', json_encode([
                'current_page' => $results->currentPage(),
                'last_page' => $results->lastPage(),
                'per_page' => $results->perPage(),
                'total' => $results->total(),
            ]));
            
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $data = $request->all();
        $model = new Employee();

        try {
            DB::beginTransaction();
            EmployeeService::create($data, $model);
            DB::commit();

            return response()->json([
                "success" => true,
                "message" => "CREATED",
                "data" => new EmployeeResource($model),
                "total" => 1,
                "status" => 201
            ], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            $results = EmployeeService::getOne($id);

            return response()->json([
                "success" => true,
                "message" => "",
                "data" => new EmployeeResource($results),
                "total" => 1,
                "status" => 200
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $model = EmployeeService::getOne($id);
        $data = $request->all();

        try {
            DB::beginTransaction();
            EmployeeService::update($data, $model);
            DB::commit();

            return response()->json([
                "success" => true,
                "message" => "UPDATED",
                "data" => new EmployeeResource($model),
                "total" => 1,
                "status" => 200
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = EmployeeService::getOne($id);
        try {
            DB::beginTransaction();
            EmployeeService::delete($id);
            DB::commit();

            return response()->json([
                "success" => true,
                "message" => "DELETED",
                "data" => [],
                "total" => 1,
                "status" => 200
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
