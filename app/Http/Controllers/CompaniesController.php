<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Facades\Company\CompanyService;
use App\Http\Requests\Company\CompanyRequest;
use App\Http\Resources\Company\CompanyResource;
use App\Http\Requests\Company\UpdateCompanyRequest;
use DB;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $criteria = $request->all();
        if(!array_key_exists('limit', $criteria))
            $criteria['limit'] = 10;
        try{
            $results = CompanyService::getList($criteria);           
            $count = CompanyService::getCount($criteria);

            // return response()->json($results);
            return CompanyResource::collection($results)->response()->header('X-Pagination', json_encode([
                'current_page' => $results->currentPage(),
                'last_page' => $results->lastPage(),
                'per_page' => $results->perPage(),
                'total' => $results->total(),
            ]));
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $data = $request->all();
        $model = new Company();

        try {
            DB::beginTransaction();
            CompanyService::create($data, $model);
            DB::commit();

            return response()->json([
                "success" => true,
                "message" => "CREATED",
                "data" => new CompanyResource($model),
                "total" => 1,
                "status" => 201
            ], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            $results = CompanyService::getOne($id);

            return response()->json([
                "success" => true,
                "message" => "",
                "data" => new CompanyResource($results),
                "total" => 1,
                "status" => 200
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $model = CompanyService::getOne($id);
        $data = $request->all();

        try {
            DB::beginTransaction();
            CompanyService::update($data, $model);
            DB::commit();

            return response()->json([
                "success" => true,
                "message" => "UPDATED",
                "data" => new CompanyResource($model),
                "total" => 1,
                "status" => 200
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = CompanyService::getOne($id);
        try {
            DB::beginTransaction();
            CompanyService::delete($id);
            DB::commit();

            return response()->json([
                "success" => true,
                "message" => "DELETED",
                "data" => [],
                "total" => 1,
                "status" => 200
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
