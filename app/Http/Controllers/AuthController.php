<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AuthRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(AuthRequest $request)
    {
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)){
            return response()->json([
                "success" => false,
                "message" => "Login failed, wrong credentials or user not found",
                "data" => "",
                "total" => 1,
                "status" => 401
            ], 401);
        }
        $user = Auth::user();
        $user['token'] = $user->createToken('Access Token')->accessToken;

        return response()->json([
            "success" => true,
            "message" => "Successfully login",
            "data" => $user,
            "total" => 1,
            "status" => 200
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            "success" => true,
            "message" => "Successfully logged out",
            "data" => [],
            "total" => 1,
            "status" => 200
        ], 200);
    }
}
