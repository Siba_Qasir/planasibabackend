<?php

namespace App\Http\Resources\Company;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $logo = '';
        if($this->logo != null){
            if (Storage::disk('public')->exists($this->logo)) {
                $logo = Storage::disk('uploads')->url($this->logo);  
            }
        }
        $response = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'website' => $this->website,
            'logo' => $logo,
            'created_at' => (!is_null($this->created_at)) ? $this->created_at->diffForHumans() : "",
            'updated_at' => (!is_null($this->updated_at)) ? $this->updated_at->diffForHumans() : ""
        ];

        return $response;
    }
}
