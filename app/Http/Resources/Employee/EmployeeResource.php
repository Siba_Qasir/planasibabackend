<?php

namespace App\Http\Resources\Employee;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Company\CompanyResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $response = [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'company_id' => $this->company_id,
            'company' => (!is_null($this->company)) ? new CompanyResource($this->company) : "",
            'created_at' => (!is_null($this->created_at)) ? $this->created_at->diffForHumans() : "",
            'updated_at' => (!is_null($this->updated_at)) ? $this->updated_at->diffForHumans() : ""
        ];

        return $response;
    }
}
