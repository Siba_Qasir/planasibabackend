<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Employee\EmployeeService;
use App\Services\Company\CompanyService;
use Illuminate\Support\Facades\App;

class MyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('EmployeeService', function () {
            return new EmployeeService();
        });
        App::bind('CompanyService', function () {
            return new CompanyService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return ['EmployeeService', 'CompanyService'];
    }
}
