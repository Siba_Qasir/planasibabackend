<?php


namespace App\Facades\Company;


use Illuminate\Support\Facades\Facade;

class CompanyService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CompanyService';
    }
}