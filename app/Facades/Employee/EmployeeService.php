<?php


namespace App\Facades\Employee;


use Illuminate\Support\Facades\Facade;

class EmployeeService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'EmployeeService';
    }
}