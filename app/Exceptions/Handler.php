<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        switch ($exception){
            case $exception instanceof ModelNotFoundException:
            {
                return response()->json([
                    "success" => false,
                    "message" => 'Model Not Found',
                    "data" => [],
                    "count" => 0,
                    "status" => 404
                ], 404);
            }
            case $exception instanceof AuthenticationException :
            {
                return response()->json([
                    "success" => false,
                    "message" => $exception->getMessage(),
                    "data" => [],
                    "count" => 0,
                    "status" => 401
                ], 401);
            }
            case $exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException:
            {
                return response()->json([
                    "success" => false,
                    "message" => 'You do not have the required authorization.', //$exception->getMessage(),
                    "data" => [],
                    "count" => 0,
                    "status" => 401
                ]);
            }
            case $exception instanceof AuthorizationException:
                {
                    return response()->json([
                        "success" => false,
                        "message" => $exception->getMessage(),
                        "data" => [],
                        "count" => 0,
                        "status" => 403
                    ], 403);
                }
            default:
            {
                return response()->json([
                    "success" => false,
                    "message" => $exception->getMessage(),//$exception->getMessage(),
                    "data" => [],
                    "count" => 0,
                    "status" => 500
                ], 500);
            }
        }
    }
}
