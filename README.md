## Quick start


### install dependencies
1- `composer install`
2- `php artisan migrate`
3- `php artisan passport:install`
4- `php artisan db:seed --class=UsersTableSeeder`
5- `php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"`
6- `php artisan migrate`
7- `php artisan make:seeder RolesTableSeeder`
8- `php artisan make:seeder ModelHasRolesTableSeeder`
### serve at localhost:8000
`php artisan serve`